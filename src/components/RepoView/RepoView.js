import React from 'react'
import {connect} from 'react-redux'
import {Grid, Row, Col} from 'react-bootstrap'

const mapStateToProps = state => ({
  githubAngularRepos: state.githubData.githubAngularRepos,
  githubRanking: state.githubData.githubRanking
})

class RepoView extends React.Component {

  render() {
    const repo = this.props.githubAngularRepos.find(
        repo =>
        repo.name === this.props.params.name
    )

    return repo !== undefined ? (
            <Grid>
              <Row>
                <Col xs={12} className="contributor-view">
                 <h1>{repo.name}</h1>
                </Col>
              </Row>
            </Grid>
        ) : ( <div>
          No Repo found
        </div>)
  }


}


export default connect(mapStateToProps)(RepoView)