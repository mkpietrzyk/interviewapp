export const sorters = {
  none: (prev, curr) => {
    let prevName = prev.login.toUpperCase();
    let curName = curr.login.toUpperCase();
    if (prevName < curName) {
      return -1
    }
    if (prevName > curName) {
      return 1
    }
    return 0
  },
  byContributions: (prev, curr) => {
    let prevContributions = prev.contributions
    let currContributions = curr.contributions
    if (prevContributions < currContributions) {
      return 1
    }
    if (prevContributions > currContributions) {
      return -1
    }
    return 0
  },
  byFollowers: (prev, curr) => {
    let prevFollowers = prev.followers
    let corrFollowers = curr.followers
    if (prevFollowers < corrFollowers) {
      return 1
    }
    if (prevFollowers > corrFollowers) {
      return -1
    }
    return 0
  },
  byGists: (prev, curr) => {
    let prevGists = prev.gists
    let currGists = curr.gists
    if (prevGists < currGists) {
      return 1
    }
    if (prevGists > currGists) {
      return -1
    }
    return 0
  }
}