import {
  RESET_SORT,
  SORT_DATA_BY_CONTRIBUTIONS,
  SORT_DATA_BY_FOLLOWERS,
  SORT_DATA_BY_GISTS
} from './actionTypes'

export const resetSort = () => dispatch => {
  dispatch({type: RESET_SORT})
}

export const sortByContributions = () => dispatch => {
  dispatch({type: SORT_DATA_BY_CONTRIBUTIONS})
}

export const sortByFollowers = () => dispatch => {
  dispatch({type: SORT_DATA_BY_FOLLOWERS})
}

export const sortByGists = () => dispatch => {
  dispatch({type: SORT_DATA_BY_GISTS})
}



