import {
  RESET_SORT,
  SORT_DATA_BY_CONTRIBUTIONS,
  SORT_DATA_BY_FOLLOWERS,
  SORT_DATA_BY_GISTS
} from './actionTypes'

const initialState = {
  selectedSort: 'none'
}

export default(state = initialState, action = {}) => {
  switch (action.type) {
    case RESET_SORT:
      return {
        ...state,
        selectedSort: state.selectedSort = 'none'
      }
    case SORT_DATA_BY_CONTRIBUTIONS:
      return {
        ...state,
        selectedSort: state.selectedSort = 'byContributions'
      }
    case SORT_DATA_BY_FOLLOWERS:
      return {
        ...state,
        selectedSort: state.selectedSort = 'byFollowers'
      }
    case SORT_DATA_BY_GISTS:
      return {
        ...state,
        selectedSort: state.selectedSort = 'byGists'
      }
    default:
      return state

  }
}