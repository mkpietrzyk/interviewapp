import {FETCH_GITHUB_CONTRIBUTOR, FETCH_GITHUB_ANGULAR_REPOS, GET_CONTRIBUTORS_RANKING, GET_CONTRIBUTOR_REPOS} from './actionTypes'

const initialState = {
  githubContributors: null,
  githubAngularRepos: null,
  githubRanking: null,
  githubContributorRepos: null
}

export default(state = initialState, action = {}) => {
  switch(action.type){
    case FETCH_GITHUB_ANGULAR_REPOS:
      return{
          ...state,
        githubAngularRepos: action.githubAngularRepos
      }
    case FETCH_GITHUB_CONTRIBUTOR:
      return{
          ...state,
        githubContributors: action.githubContributors
      }
    case  GET_CONTRIBUTORS_RANKING:
      return{
          ...state,
        githubRanking: action.githubRanking
      }
    case GET_CONTRIBUTOR_REPOS:
      return{
          ...state,
        githubContributorRepos: action.githubContributorRepos
      }
    default:
      return state
  }
}