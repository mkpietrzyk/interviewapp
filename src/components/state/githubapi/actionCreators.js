import {
  FETCH_GITHUB_ANGULAR_REPOS,
  GET_CONTRIBUTORS_RANKING,
  GET_CONTRIBUTORS_RANKING_BEGIN,
  GET_CONTRIBUTORS_RANKING_END,
  GET_CONTRIBUTOR_REPOS,
  GET_CONTRIBUTOR_REPOS_BEGIN,
  GET_CONTRIBUTOR_REPOS_END
} from './actionTypes'

import async from 'async'

let arrayOfTasks = []
let contributorFetchArray = []
let namesArray = []
let contributorsRankingArray = []

const fetchContributor = (name, contributions, callback) => {
  fetch('https://api.github.com/users/' + name + '?client_id=116a3b1e8542ddce59d4&client_secret=8b01ac54843e3834339cc45edd44dd4fde15ce3f').then(
      (response) => {
        return response.json()
      }
  ).then(
      (contributorData) => {
        namesArray.push({
          login: contributorData.login,
          name: contributorData.name,
          location: contributorData.location,
          bio: contributorData.bio,
          image: contributorData.avatar_url,
          contributions: contributions,
          followers: contributorData.followers,
          repos: contributorData.public_repos,
          gists: contributorData.public_gists
        })
      }
  ).then(
      () => {
        if (typeof callback === "function") callback();
      }
  ).catch(
      (errorFetching) => {
        console.log('failed to fetch: ', errorFetching)
      }
  )
}

const fetchRanking = (name, callback) => {
  fetch('https://api.github.com/repos/angular/' + name + '/contributors?per_page=100&client_id=116a3b1e8542ddce59d4&client_secret=8b01ac54843e3834339cc45edd44dd4fde15ce3f').then(
      (response) => {
        return response.json()
      }
  ).then(
      (contributorsData) => {
        contributorsData.map(
            contributor => {
              return contributorFetchArray.push((callback) => {
                fetchContributor(contributor.login, contributor.contributions, () => {
                  callback()
                })
              })
            }
        )
      }
  ).then(
      () => {
        if (typeof callback === "function") callback();
      }
  ).catch(
      (errorFetching) => {
        console.log('failed to fetch: ', errorFetching)
      }
  )
}

export const fetchAllRepos = () => dispatch => {
  fetch('https://api.github.com/orgs/angular/repos?per_page=70&client_id=116a3b1e8542ddce59d4&client_secret=8b01ac54843e3834339cc45edd44dd4fde15ce3f').then(
      (response) => {
        return response.json()
      }
  ).then(
      (githubRepoData) => {
        dispatch({type: GET_CONTRIBUTORS_RANKING_BEGIN})
        dispatch({type: FETCH_GITHUB_ANGULAR_REPOS, githubAngularRepos: githubRepoData})

        githubRepoData.map(
            (githubRepo) => {
              return arrayOfTasks.push((callback) => {
                fetchRanking(githubRepo.name, () => {
                  callback()
                })
              })
            }
        )

        async.parallel(arrayOfTasks, () => {
          async.parallel(contributorFetchArray, () => {
            namesArray.filter(function (contributor) {
              return contributor.login !== 'NgDashboard';
            }).map(function (contributor) {
              if (!this[contributor.login]) {
                this[contributor.login] = {
                  login: contributor.login,
                  name: contributor.name,
                  location: contributor.location,
                  bio: contributor.bio,
                  image: contributor.image,
                  contributions: 0,
                  followers: contributor.followers,
                  repos: contributor.repos,
                  gists: contributor.gists
                }
                contributorsRankingArray.push(this[contributor.login])
              }
              this[contributor.login].contributions += contributor.contributions
            }, Object.create(null))

            dispatch({type: GET_CONTRIBUTORS_RANKING, githubRanking: contributorsRankingArray})
            dispatch({type: GET_CONTRIBUTORS_RANKING_END})
          })
        })
      }
  ).catch((errorFetching) => {
        console.log('failed to fetch: ', errorFetching)
      }
  )
}

export const fetchContributorRepos = (name) => dispatch => {
  fetch('https://api.github.com/users/' + name + '/repos?per_page=100&client_id=116a3b1e8542ddce59d4&client_secret=8b01ac54843e3834339cc45edd44dd4fde15ce3f').then(
      (response) => {
        return response.json()
      }
  ).then(
      (reposData) => {
        dispatch({type: GET_CONTRIBUTOR_REPOS_BEGIN})
        dispatch({type: GET_CONTRIBUTOR_REPOS, githubContributorRepos: reposData})
        dispatch({type: GET_CONTRIBUTOR_REPOS_END})
      }
  ).catch(
      (errorFetching) => {
        console.log('failed to fetch: ', errorFetching)
      }
  )
}