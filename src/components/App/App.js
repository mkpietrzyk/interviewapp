import React from 'react';
import './App.css';

export default (props) => (
    <div>{props.children}</div>
)