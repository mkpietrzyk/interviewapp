import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import {Grid, Row, Col, Button} from 'react-bootstrap'
import {MdLoop} from 'react-icons/lib/md'
import {fetchContributorRepos} from '../state/githubapi/actionCreators'

import './ContributorView.css'


const mapStateToProps = state => ({
  githubRanking: state.githubData.githubRanking,
  githubContributorRepos: state.githubData.githubContributorRepos
})

const mapDispatchToProps = dispatch => ({
  fetchContributorRepos: (name) => dispatch(fetchContributorRepos(name))
})


class ContributorView extends React.Component {
  componentDidMount() {
    this.props.fetchContributorRepos(this.props.params.login)
  }

  render() {
    const contributor = this.props.githubRanking.find(
        contributor =>
        contributor.login === this.props.params.login
    )

    return contributor !== undefined ? (
            <Grid>
              <Row>
                <Col xs={12} className="contributor-view">
                  <Col xs={12} className="contributor-view__details">

                    <Row>
                      <Col xs={12}>
                        <Link to={'/'}>
                          <Button bsStyle="primary" bsSize="small">Go back</Button>
                        </Link>
                      </Col>
                      <h1>{contributor.login}</h1>
                      <Col xs={12} md={6} className="contributor-view__details__image-holder">
                        <img src={contributor.image} alt={contributor.login}/>
                      </Col>
                      <Col xs={12} md={6}>
                        <p>Full Name: {contributor.name}</p>
                        <p>Location: {contributor.location}</p>
                        <p>Followers: {contributor.followers}</p>
                        <p>Repositories: {contributor.repos}</p>
                        <p>Gists: {contributor.gists}</p>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={12}>
                    <h1>Angular Repositories</h1>
                    <Row className="repos-list">
                      {this.props.githubContributorRepos !== null ?
                          this.props.githubContributorRepos.map(
                              repo => ({
                                ...repo
                              })
                          ).filter((repo) => {
                            return repo.name.includes('angular')
                          }).map(
                              repo =>
                                  <Col key={repo.name} xs={6} md={4} lg={2} className="repos-list__item">
                                    <Link className="repo-list__item__link" to={'/repo/' + repo.name}>
                                      <div className="repo-list__item__wrapper">
                                        <p>{repo.name}</p>
                                      </div>
                                    </Link>
                                  </Col>
                          ) :
                          <div className="repos-list__loading">
                            <MdLoop className="loading-screen__icon loading-screen__icon--loop"/>
                            <p>Loading repos...</p>
                          </div>
                      }
                    </Row>
                  </Col>
                </Col>
              </Row>
            </Grid>
        ) : ( <div>
          No contributor found
        </div>)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContributorView)