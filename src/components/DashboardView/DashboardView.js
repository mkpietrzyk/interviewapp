import React from 'react'
import {Link} from 'react-router'
import {Button, Col, Grid, Row} from 'react-bootstrap'
import {connect} from 'react-redux'
import {fetchAllRepos} from '../state/githubapi/actionCreators'
import {resetSort, sortByContributions, sortByFollowers, sortByGists} from '../state/sort/acitonCreators'
import {sorters} from '../utilities/sorters'

import {MdLoop, MdCloud} from 'react-icons/lib/md'

import "./DashboardView.css"
import Logo from '../../angular_solidBlack.svg'

const mapStateToProps = state => ({
  githubContributors: state.githubData.githubContributors,
  githubAngularRepos: state.githubData.githubAngularRepos,
  githubRanking: state.githubData.githubRanking,
  sorting: state.sortedRankingData.selectedSort
})

const mapDispatchToProps = dispatch => ({
  fetchAllRepos: () => dispatch(fetchAllRepos()),
  resetSort: () => dispatch(resetSort()),
  sortByContributions: () => dispatch(sortByContributions()),
  sortByFollowers: () => dispatch(sortByFollowers()),
  sortByGists: () => dispatch(sortByGists())
})



class DashboardView extends React.Component {

  componentWillMount() {
    if(this.props.githubRanking === null) {
      this.props.fetchAllRepos()
    }
  }

  render() {
    if (this.props.githubRanking === null) {
      return (
          <div className="sky">
            <MdCloud className="sky__cloud sky__cloud--1"/>
            <MdCloud className="sky__cloud sky__cloud--2"/>
            <div className="loading-screen">

              <div className="loading-screen__label">
                <img src={Logo} className="loading-screen__icon loading-screen__icon--App-Logo" alt="Logo-icon"/>
                <MdLoop className="loading-screen__icon loading-screen__icon--loop"/>
                <p>We're loading. Hold on...</p>
              </div>
            </div>
          </div>
      )
    }
    return (
        <div className="ranking-view">
          <Grid>
            <Row>
              <Col xs={12}>
                <div className="ranking-view__header">
                  <img src={Logo} className="loading-screen__icon loading-screen__icon--App-Logo" alt="Loading-icon"/>
                  <h1>Angular Repositories Contributors Ranking</h1>
                  <Button bsStyle="danger" bsSize="small" onClick={this.props.resetSort}>Reset sort</Button>
                  <Button bsStyle="primary" bsSize="small" onClick={this.props.sortByContributions}>Most
                    Contributions</Button>
                  <Button bsStyle="primary" bsSize="small" onClick={this.props.sortByFollowers}>Most Followers</Button>
                  <Button bsStyle="primary" bsSize="small" onClick={this.props.sortByGists}>Most Gists</Button>
                </div>
                <ul className="contributors-list">
                  {this.props.githubRanking.map(
                      contributor => ({
                        ...contributor,
                      })
                  ).sort(
                      sorters[this.props.sorting]
                  ).map(
                      (contributor, value) =>
                          <li key={value} className='contributors-list__item'>
                            <Link className="contributors-list__item__link" to={'/user/' + contributor.login}>
                              <div className="contriburors-list__image-holder">
                                <img src={contributor.image} alt={contributor.login}/>
                              </div>
                              <Row className="contributors-list__item__row">
                                <Col xs={12}>
                                  <p className="contributors-list__item__name"><b>{contributor.login}</b></p>
                                </Col>
                                <Col xs={12}>
                                  <p className="contributors-list__item__contributions">
                                    <span>Contributions for Angular Repositories</span>
                                    <span>
                                      <b>{contributor.contributions}</b>
                                    </span>
                                  </p>
                                </Col>
                                <Col xs={12} md={4}>
                                  <p className="contributors-list__item__followers">
                                    <span>Followers</span>
                                    <span>
                                      <b>{contributor.followers}</b>
                                    </span>
                                  </p>
                                </Col>
                                <Col xs={12} md={4}>
                                  <p className="contributors-list__item__repos">
                                    <span>Repositories</span>
                                    <span>
                                      <b>{contributor.repos}</b>
                                    </span>
                                  </p>
                                </Col>
                                <Col xs={12} md={4}>
                                  <p className="contributors-list__item__gists">
                                    <span>Gists</span>
                                    <span>
                                      <b>{contributor.gists}</b>
                                    </span>
                                  </p>
                                </Col>
                              </Row>
                            </Link>
                          </li>
                  )}
                </ul>
              </Col>
            </Row>
          </Grid>
        </div>
    )
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(DashboardView)