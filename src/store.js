import {compose, createStore, combineReducers, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import githubReducer from './components/state/githubapi/reducer'
import sortReducer from './components/state/sort/reducer'

const reducer = combineReducers({
  githubData: githubReducer,
  sortedRankingData: sortReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const enhancer = composeEnhancers(
    applyMiddleware(
        thunk
    )
)

const store = createStore(reducer, enhancer);


export default store

// puszin kocha puszinke <3