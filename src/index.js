import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import './index.css';

import {Provider} from 'react-redux'
import {Router, Route, IndexRoute, browserHistory} from 'react-router'
import store from './store'

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";

import {DashboardView} from './components/DashboardView'
import {ContributorView} from './components/ContributorView'
import {RepoView} from './components/RepoView'


ReactDOM.render(
  <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="/" component={App}>
          <IndexRoute component={DashboardView}/>
          <Route path="/user/:login" component={ContributorView}/>
          <Route path="/repo/:name" component={RepoView}/>
        </Route>
      </Router>
  </Provider>,
  document.getElementById('root')
);
