# Interview Application

### Michał Pietrzyk

##### 90% done / Please see list of libraries or 'achievement list' below

### Project tasks achieved

* React App installation and cofiguration 
* Installed npm modules: redux, redux thunk, react-router(ver. 3.x), react-bootstrap
* Simple routing with connection to redux 
* Async fetching from githubApi
* deleted .idea folder 
    * reason: being unnecessary and added by .gitignore mistake
* Main logic for fetching for all Angular repos
    * Function awaits for all repositories data to download
    * Working sorting for DESC order (can be improved for toggling with ASC in future)
    * Working links to Contributor's personal page
* Fetching for Contributor's angular repositories he/she contributed to into his/hers personal page
* Simple styling and animations used with CSS3 (SCSS files) 
    * Color pallete : http://www.flatuicolorpicker.com/
        
    
    
### To do
    
* Due to some issues, Repo's personal page is not working properly
    
    * Possible fix: Redesign global fetching for all repositories for both code duplication prevention and optimization purposes


### Featured libraries/frameworks

* Core

    * React + Redux (with Router)
    * acync library : https://caolan.github.io/async/index.html
          
          
* Styling

    * react-bootstrap
    * react-icons
    * styling done using SCSS files
        
        
        

 
 
### How to run

    * clone repository
    * install packages via npm install
    * run project via npm start


### Commits Shortcut Legend:

    * F - Feature 
    * S - Styling 
    * R - Readme fix